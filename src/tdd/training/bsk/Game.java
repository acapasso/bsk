package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	private ArrayList<Frame> frames= new ArrayList<Frame>(10);
	int firstBonusThrow;
	int secondBonusThrow;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game(){
		this.frames=frames;
		this.firstBonusThrow=0;
		this.secondBonusThrow=0;
		
	}
	
	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		this.frames.add(frame);
				
	}


	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return this.frames.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow=secondBonusThrow;
		
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException{
		int score=0;
		int count_strike=0;
		
		for(Frame f1: frames) {
			if(f1.isStrike() && this.firstBonusThrow==10 && this.secondBonusThrow==10)
				count_strike++;
		}
		
		for(int i=0; i<frames.size();i++){
			Frame f=frames.get(i);
			
			if(f.isStrike() && count_strike==10) {
				score=300;
			}
			else if(f.isSpare() && i==frames.size()-1) {
				f.setBonus(this.firstBonusThrow);
				score+=f.getScore();
			}
			else if(f.isSpare()) {
				f.setBonus(frames.get(i+1).getFirstThrow());
				score+=f.getScore();
			}
			else if(f.isStrike() && i==frames.size()-1) {
				f.setBonus(this.firstBonusThrow+this.secondBonusThrow);
				score+=f.getScore();
			}
			else if(f.isStrike() && frames.get(i+1).isStrike()) {
				f.setBonus(frames.get(i+1).getScore()+frames.get(i+2).getFirstThrow());
				score+=f.getScore();
			}
			else if(f.isStrike())
			{
				f.setBonus(frames.get(i+1).getScore());
				score+=f.getScore();
			}
			else
				score+=f.getScore();
		}
		return score;
	}
	
}