package tdd.training.bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class FrameTest {


	@Test
	//First user story
	public void setFrame() throws BowlingException {
		int firstThrow=2;
		int secondThrow =4;
		Frame f= new Frame(firstThrow, secondThrow);
		assertEquals(2,f.getFirstThrow());
		assertEquals(4,f.getSecondThrow());
	}
	
	@Test
	//Second user story
	public void getScore() throws BowlingException {
		int firstThrow=2;
		int secondThrow =6;
		Frame f= new Frame(firstThrow, secondThrow);
		assertEquals(8,f.getScore());
	}

	@Test
	//Third user story
	public void defineAGame() throws BowlingException{
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(7,game.getFrameAt(2).getFirstThrow());
		assertEquals(2,game.getFrameAt(2).getSecondThrow());
	}
	
	@Test
	//Forth user story
	public void gameScore() throws BowlingException{
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(81,game.calculateScore());
	}
	
	@Test
	//Fifth user story
	public void gameSpare() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(88,game.calculateScore());
		
	}
	
	
	@Test
	//Sixthy user story
	public void isStrike() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(94, game.calculateScore());
		
	}
	
	@Test
	//Seventh user story
	public void isSpareAndIsStrike() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(103, game.calculateScore());
	}
	@Test
	//Eight user story
	public void MultipleStrikes() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(112, game.calculateScore());
		
	}

	@Test
	//Ninth user story
		public void MultipleSpare() throws BowlingException {
			Game game= new Game();
			game.addFrame(new Frame(8,2));
			game.addFrame(new Frame(5,5));
			game.addFrame(new Frame(7,2));
			game.addFrame(new Frame(3,6));
			game.addFrame(new Frame(4,4));
			game.addFrame(new Frame(5,3));
			game.addFrame(new Frame(3,3));
			game.addFrame(new Frame(4,5));
			game.addFrame(new Frame(8,1));
			game.addFrame(new Frame(2,6));
			
			assertEquals(98, game.calculateScore());
		}
	@Test
	//Tenth user story
		public void LastFrameIsSpare() throws BowlingException {
			Game game= new Game();
			game.addFrame(new Frame(1,5));
			game.addFrame(new Frame(3,6));
			game.addFrame(new Frame(7,2));
			game.addFrame(new Frame(3,6));
			game.addFrame(new Frame(4,4));
			game.addFrame(new Frame(5,3));
			game.addFrame(new Frame(3,3));
			game.addFrame(new Frame(4,5));
			game.addFrame(new Frame(8,1));
			game.addFrame(new Frame(2,8));
			game.setFirstBonusThrow(7);
			
			assertEquals(90, game.calculateScore());
		}
	
	@Test
	//Eleventh user story
		public void LastFrameIsStrike() throws BowlingException {
			Game game= new Game();
			game.addFrame(new Frame(1,5));
			game.addFrame(new Frame(3,6));
			game.addFrame(new Frame(7,2));
			game.addFrame(new Frame(3,6));
			game.addFrame(new Frame(4,4));
			game.addFrame(new Frame(5,3));
			game.addFrame(new Frame(3,3));
			game.addFrame(new Frame(4,5));
			game.addFrame(new Frame(8,1));
			game.addFrame(new Frame(10,0));
			game.setFirstBonusThrow(7);
			game.setSecondBonusThrow(2);
			
			assertEquals(92, game.calculateScore());
		}
	@Test
	//Twelve user story
	public void BestScore() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}
}
